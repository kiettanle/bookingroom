﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Security;

using DHTMLX.Common;
using DHTMLX.Scheduler;
using DHTMLX.Scheduler.Authentication;
using DHTMLX.Scheduler.Controls;
using DHTMLX.Scheduler.Data;
using RoomBookingCalendar.Models;
using System.Web.Script.Serialization;

namespace RoomBookingCalendar.Controllers
{
    public class BookingController : Controller
    {
        protected void _ConfigureLightbox(DHXScheduler sched, MyEventsDataContext context)
        {
            var select = new LightboxSelect("room_id", "Room");
            select.AddOptions(context.Rooms);
            sched.Lightbox.AddDefaults();
            sched.Lightbox.Items.Insert(1, select); 
        }

        protected void _ConfigureViewsAndTemplates(DHXScheduler sched, MyEventsDataContext context)
        {
            sched.Views.Items.RemoveAt(2);//remove DayView
         
            var units = new UnitsView("rooms", "room_id");
         
            units.Label = "Phòng";
            units.AddOptions(context.Rooms);          
            sched.Views.Add(units);
            sched.Views.Add(new WeekAgendaView());

            
        }

        protected void _ConfigureScheduler(DHXScheduler sched, MyEventsDataContext context)
        {
            sched.SetEditMode(EditModes.OwnEventsOnly, EditModes.AuthenticatedOnly);
            sched.Extensions.Add(SchedulerExtensions.Extension.Collision);
            sched.Extensions.Add(SchedulerExtensions.Extension.Limit);
           
            

            sched.Config.first_hour = 7;
            sched.Config.last_hour = 20;
            sched.XY.scroll_width = 0;
            sched.Config.time_step = 30;
            sched.Config.multi_day = true;
            sched.Config.limit_time_select = true;

            sched.Config.cascade_event_display = true;


            sched.AfterInit = new List<string>() { "attachRules();" };

            sched.LoadData = true;
            sched.PreventCache();

            if (Request.IsAuthenticated)
            {
                sched.EnableDataprocessor = true;
            }  
        }
        protected void _HandleAuthorization(DHXScheduler sched, MyEventsDataContext context)
        {
            if (Request.IsAuthenticated)
            {
                var user = context.Users.SingleOrDefault(u => u.UserId == (Guid)Membership.GetUser().ProviderUserKey);
                sched.SetUserDetails(user, "UserId");
                sched.Authentication.EventUserIdKey = "user_id";

                sched.InitialValues.Add("textColor", user.color);
                sched.InitialValues.Add("room_id", context.Rooms.First().key);
            }

        }


        public ActionResult Index()
        {
            var sched = new DHXScheduler();
            var context = new MyEventsDataContext();
            sched.Skin = DHXScheduler.Skins.Glossy;
            sched.Codebase = Url.Content("~/Scripts/dhtmlxScheduler");
            sched.SaveAction = Url.Content("~/Booking/Save");
            sched.DataAction = Url.Content("~/Booking/Data");

            _ConfigureScheduler(sched, context);
            _ConfigureViewsAndTemplates(sched, context);
            _ConfigureLightbox(sched, context);
            _HandleAuthorization(sched, context);

                      
            var serializer = new JavaScriptSerializer();
            var rooms = serializer.Serialize(context.Rooms);
            var colors = serializer.Serialize(
                (from us in context.Users select new { id = us.UserId.ToString(), color = us.color })
                    .ToDictionary(t => t.id.ToLower(), t => t.color));


            return View(new CalendarModel() { Rooms = rooms, Colors = colors, Scheduler = sched });
        }
        public ActionResult Tutorial()
        {
            return View();
        }
        public ActionResult Data()
        {
            return (new SchedulerAjaxData((new MyEventsDataContext()).Events));         
        }

		public ActionResult Save(int? id, FormCollection actionValues)
		{
			var action = new DataAction(actionValues);
            var changedEvent = (Event)DHXEventsHelper.Bind(typeof(Event), actionValues);
            MyEventsDataContext data = new MyEventsDataContext();

            if (Request.IsAuthenticated && changedEvent.user_id == (Guid)Membership.GetUser().ProviderUserKey && changedEvent.start_date > DateTime.Now)
            {
               
                try
                {
                    switch (action.Type)
                    {
                        case DataActionTypes.Insert:
                            data.Events.InsertOnSubmit(changedEvent);
                            break;
                        case DataActionTypes.Delete:
                            changedEvent = data.Events.SingleOrDefault(ev => ev.id == action.SourceId);
                            data.Events.DeleteOnSubmit(changedEvent);
                            break;
                        default:// "update"                          
                            var eventToUpdate = data.Events.SingleOrDefault(ev => ev.id == action.SourceId);
                            DHXEventsHelper.Update(eventToUpdate, changedEvent, new List<string>() { "id" });
                            break;
                    }
                    data.SubmitChanges();
                    action.TargetId = changedEvent.id;
                }
                catch(Exception a)
                {
                    action.Type = DataActionTypes.Error;
                }
            }
            else
            {
                action.Type = DataActionTypes.Error;
            }

            return new AjaxSaveResponse(action);
		}

    }
}
