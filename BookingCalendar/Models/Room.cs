namespace RoomBookingCalendar.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Room
    {
        [Key]
        public int key { get; set; }

        [StringLength(128)]
        public string label { get; set; }
    }
}
