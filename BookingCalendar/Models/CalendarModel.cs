﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DHTMLX.Scheduler;
namespace RoomBookingCalendar.Models
{
    public class CalendarModel
    {
        public string Rooms { get; set; }
        public string Colors { get; set; }
        public DHXScheduler Scheduler { get; set; }
    }
}