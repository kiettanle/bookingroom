namespace RoomBookingCalendar.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Event
    {
        public int id { get; set; }

        [Column(TypeName = "text")]
        public string text { get; set; }

        public DateTime? start_date { get; set; }

        public DateTime? end_date { get; set; }

        public int? room_id { get; set; }

        public Guid? user_id { get; set; }
    }
}
