Booking Calendar Demo in ASPX with DHTMLX Scheduler .NET 

Live demo http://booking-app.scheduler-net.com

Read tutorial http://blog.scheduler-net.com/post/2012/04/13/online-booking-calendar-tutorial-asp-net.aspx

To purchase Commercial/Enterprise Licence visit http://scheduler-net.com/license.aspx

(c) DHTMLX Ltd. 